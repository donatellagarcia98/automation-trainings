package TestCases;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;

import com.excel.lib.util.Xls_Reader;

import ObjectRepository.FormPage;
import ObjectRepository.SubmitedFormPage;

public class Test1 {
	
	
	@Test(dataProvider="testdata")
	public void Test1FillAndSubmit(String name, String lastName, String JobTitle){
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\A\\Desktop\\SeleniumCourse\\geckodriver-v0.30.0-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		// Declaring an Explicit Wait Object globaly 
		WebDriverWait w = new WebDriverWait(driver, 5);
			
		// Get to WebPage
		driver.get("https://formy-project.herokuapp.com/form");
		// Call the Page Object Model file and create an object
		FormPage fp = new FormPage(driver);
		SubmitedFormPage sp = new SubmitedFormPage(driver);
		
		// Fill written data
		fp.FirstName().clear();
		fp.FirstName().sendKeys(name);
		fp.LastName().clear();
		fp.LastName().sendKeys(lastName);
		fp.JobTitle().clear();
		fp.JobTitle().sendKeys(JobTitle);
		// Select optional data
		
		fp.HighS().click();
		fp.Sex().click();
		// Select from dropdown menu
		fp.Years().click();
		// Select todays date in calendar
		fp.Date().click();
		fp.TodayDate().click();
		// CLICK SUBMIT BUTTON
		fp.SubmitBtn().click();		
		
		w.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='alert alert-success']")));
		
		AssertJUnit.assertEquals(sp.PageH1().getText(), "Thanks for submitting your form");
		
		driver.close();
	}
	
	
	
	@DataProvider(name = "testdata")
	public Object[][] getData() {
		Xls_Reader reader = new Xls_Reader("C:\\Users\\A\\Desktop\\SeleniumCourse\\Reference.xlsx");
		String sheetName = "Data";
		
		int rows = reader.getRowCount(sheetName);
		
		Object[][] data = new Object[rows-1][3];
		
		for(int i = 2;i<=rows;i++) {
			data[i-2][0] = reader.getCellData(sheetName, 0, i);
			data[i-2][1] = reader.getCellData(sheetName, 1, i);
			data[i-2][2] = reader.getCellData(sheetName, 2, i);
		}
		return data;
	}

	
	
	
	
}
