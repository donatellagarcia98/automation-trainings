package TestCases;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;

import com.excel.lib.util.Xls_Reader;

import ObjectRepository.FormPage;
import ObjectRepository.SubmitedFormPage;

public class Test1 {
	
	
	@Test(dataProvider="testdata")
	public void Test1FillAndSubmit(String name, String lastName, String JobTitle) throws IOException{
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\A\\Desktop\\SeleniumCourse\\geckodriver-v0.30.0-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		// Declaring an Explicit Wait Object globaly 
		WebDriverWait w = new WebDriverWait(driver, 5);
		// Initializing interface for screenshots 
		TakesScreenshot ts = (TakesScreenshot) driver;
		// Sets names for each screenshot 
		String FileSelect = System.getProperty("user.dir") + "\\screenshots\\" + name + lastName + "Select" + ".png";
		String FileCheckbox = System.getProperty("user.dir") + "\\screenshots\\" + name + lastName + "Checkbox" + ".png";
		String FileDatePicker = System.getProperty("user.dir") + "\\screenshots\\" + name + lastName + "Calendar" + ".png";
		String FileSubmitPage = System.getProperty("user.dir") + "\\screenshots\\" + name + lastName + "Submit" + ".png";
		
		// Get to WebPage
		driver.get("https://formy-project.herokuapp.com/form");
		// Call the Page Object Model file and create an object
		FormPage fp = new FormPage(driver);
		SubmitedFormPage sp = new SubmitedFormPage(driver);
		
		// Fill written data
		fp.FirstName().clear();
		fp.FirstName().sendKeys(name);
		fp.LastName().clear();
		fp.LastName().sendKeys(lastName);
		fp.JobTitle().clear();
		fp.JobTitle().sendKeys(JobTitle);
		// Select optional data
		
		fp.HighS().click();
		fp.Sex().click();
		// Screenshot for the Checkbox Element
		File source2 = ts.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(source2, new File(FileCheckbox));
		// Select from dropdown menu
		fp.Years().click();
		// Screenshot for the Select Object
		File source1 = ts.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(source1, new File(FileSelect));
		// Select todays date in calendar
		fp.Date().click();
		fp.TodayDate().click();
		// Screenshot for the Datepicker Element
		File source3 = ts.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(source3, new File(FileDatePicker));
		// CLICK SUBMIT BUTTON
		fp.SubmitBtn().click();		
		// Screenshot for the submitted button
		File source4 = ts.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(source4, new File(FileSubmitPage));
		// Waiting until Element located
		w.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='alert alert-success']")));
		// Asserting message
		AssertJUnit.assertEquals(sp.PageH1().getText(), "Thanks for submitting your form");
		// Closing driver
		driver.close();
	}
	
	@DataProvider(name = "testdata")
	public Object[][] getData() {
		// Creating a xlsx object for excel file reading
		Xls_Reader reader = new Xls_Reader("C:\\Users\\A\\Desktop\\SeleniumCourse\\Reference.xlsx");
		String sheetName = "Data";
		
		int rows = reader.getRowCount(sheetName);
		
		// Creating an object to read the rows of the file 
		Object[][] data = new Object[rows-1][3];
		
		for(int i = 2;i<=rows;i++) {
			data[i-2][0] = reader.getCellData(sheetName, 0, i);
			data[i-2][1] = reader.getCellData(sheetName, 1, i);
			data[i-2][2] = reader.getCellData(sheetName, 2, i);
		}
		return data;
	}
	
}
