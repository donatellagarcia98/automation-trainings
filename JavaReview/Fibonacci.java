import java.util.Arrays;

public class Fibonacci {
    public static void main(String[] args) {
        printFibonacci(8);
        printFibonacci(25);
    }
    public static void printFibonacci(int Limit){
        int[] Serie = new int[Limit];

        int PrevNum = 1;
        for(int i = 0 ; i<Limit ; i++){
            Serie[i] = PrevNum;
            if(i>1){
                Serie[i] = Serie[i-1] + Serie[i-2];
            }
        }
        System.out.println(Arrays.toString(Serie));
    }
}
