import java.util.Scanner;

public class Concatenation {
    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter 2 strings to concatenate");
        String str1 = scan.next();
        String str2 = scan.next();

        System.out.println(str1 + str2);
    }
}
