public class GreatestNumber {
    public static void main(String[] args) {
        System.out.println(GreatestNum(23,12,19));
        System.out.println(GreatestNum(122,78,377));
    }

    public static int GreatestNum(int num1, int num2, int num3){
        int[] numbers = {num1,num2,num3};
        int Great = 0;
        for(int i = 0 ; i < numbers.length ; i++)
        {
            if(numbers[i]>Great){
                Great = numbers[i];
            }
        }
        return Great;
    }
}
