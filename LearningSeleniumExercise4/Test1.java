package TestCases;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ObjectRepository.FormPage;
import ObjectRepository.SubmitedFormPage;

public class Test1 {
	
	@Test
	public void Test1FillAndSubmit(){
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\A\\Desktop\\SeleniumCourse\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		// Declaring an Explicit Wait Object globaly 
		WebDriverWait w = new WebDriverWait(driver, 5);
		
		// Get to WebPage
		driver.get("https://formy-project.herokuapp.com/form");
		// Call the Page Object Model file and create an object
		FormPage fp = new FormPage(driver);
		SubmitedFormPage sp = new SubmitedFormPage(driver);
		
		// Fill written data
		fp.FirstName().sendKeys("Donatella");
		fp.LastName().sendKeys("García");
		fp.JobTitle().sendKeys("IT Trainee");
		// Select optional data
		
		fp.HighS().click();
		fp.Sex().click();
		// Select from dropdown menu
		fp.Years().click();
		// Select todays date in calendar
		fp.Date().click();
		fp.TodayDate().click();
		// CLICK SUBMIT BUTTON
		fp.SubmitBtn().click();		
		
		w.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='alert alert-success']")));
		Assert.assertEquals(sp.PageH1().getText(), "Thanks for submitting your form");
		
	}

}
