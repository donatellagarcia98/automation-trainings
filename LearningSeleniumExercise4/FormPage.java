package ObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FormPage {
	WebDriver driver;
	// Creating a constructor
	public FormPage(WebDriver driver) {
		this.driver = driver;		
	}	
	
	By FirstName = By.xpath("//input[@id = 'first-name']");
	By LastName  = By.xpath("//input[@id = 'last-name']");
	By JobTitle  = By.xpath("//input[@id = 'job-title']");
	
	By HighS     = By.cssSelector("div[class='input-group'] div input[id='radio-button-2']");
	By Sex       = By.xpath("//input[@id = 'checkbox-2']");
	By Years     = By.cssSelector("select[id='select-menu'] option[value='1']");
	By Date      = By.xpath("//input[@id = 'datepicker']");
	By TodayDate = By.cssSelector("td[class='today day']");
	By SubmitBtn = By.xpath("//a[@class='btn btn-lg btn-primary']");	
	
	public WebElement FirstName() {
		return driver.findElement(FirstName);
	}
	public WebElement LastName() {
		return driver.findElement(LastName);
	}
	public WebElement JobTitle() {
		return driver.findElement(JobTitle);
	}
	public WebElement HighS() {
		return driver.findElement(HighS);
	}
	public WebElement  Sex() {
		return driver.findElement( Sex);
	}
	public WebElement Years() {
		return driver.findElement(Years);
	}
	public WebElement Date() {
		return driver.findElement(Date);
	}
	public WebElement SubmitBtn() {
		return driver.findElement(SubmitBtn);
	}
	public WebElement TodayDate() {
		return driver.findElement(TodayDate);
	}
	
	


}
