package ObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SubmitedFormPage {
	WebDriver driver;
	// Creating a constructor
	public SubmitedFormPage(WebDriver driver) {
		this.driver = driver;		
	}	
	By Submitted = By.xpath("//div[@class='alert alert-success']");
	By PageH1    = By.cssSelector("html h1");
	
	public WebElement Submitted() {
		return driver.findElement(Submitted);
	}
	public WebElement PageH1() {
		return driver.findElement(PageH1);
	}

}
