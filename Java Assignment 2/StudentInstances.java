import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;

public class StudentInstances {
    public static void main(String[] args) throws ParseException{
        int randId1 = (int)(Math.random() * 10000);
        int randId2 = (int)(Math.random() * 10000);
        int randId3 = (int)(Math.random() * 10000);

        Student s1 = new Student(randId1, "Metztli", "Donatella", "Garcia", "16-01-1998");
        Student s2 = new Student(randId2, "Victor", "Hugo", "Hernandez", "14-05-1998");
        Student s3 = new Student(randId3, "Ulises", "Jaime", "Vargaz", "07-09-1995");




        System.out.println("Student:   " + s1.getFullName() + "     ID:   " + s1.getID() + "     DOB:   " + s1.changeDOB());
        System.out.println("Student:   " + s2.getFullName() + "     ID:   " + s2.getID() + "     DOB:   " + s2.changeDOB());
        System.out.println("Student:   " + s3.getFullName() + "     ID:   " + s3.getID() + "     DOB:   " + s3.changeDOB());
    }

}
