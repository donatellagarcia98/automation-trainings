public class Exceptions {
    public static void main(String[] args){
        div(10,0);
        ConcStr(null, "name");
    }
    public static void div(int num, int den){
        try{
            System.out.println(num/den);
        }
        catch(ArithmeticException e){
            System.out.println("Error: undefined division by 0");
        }
    }
    public static void ConcStr(String str1, String str2){
        try {
            System.out.println(str1.concat(str2));
        }
        catch (Exception e){
            System.out.println("Error: Invalid concatenation");
        }
    }
}
