public class FibonacciRe {
    public static void main(String[] args){
        RFibonacci(10);
        System.out.println(" ");
        RFibonacci(5);
    }
    public static void RFibonacci(int count){
        if (count == 0){
            return;}
        int[] serie = Serie(count);
        System.out.print(serie[count-1] + ", ");

        RFibonacci(count-1);
    }
    public static int[] Serie(int Limit){
        int[] Serie = new int[Limit];

        int PrevNum = 1;
        for(int i = 0 ; i<Limit ; i++){
            Serie[i] = PrevNum;
            if(i>1){
                Serie[i] = Serie[i-1] + Serie[i-2];
            }
        }

        return Serie;
    }
}
