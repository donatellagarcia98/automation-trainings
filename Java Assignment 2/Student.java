public class Student {
    private int ID;
    private String FirstName;
    private String MiddleName;
    private String LastName;
    private String BirthDate;

    public Student(int ID, String FirstName, String MiddleName, String LastName, String BirthDate){
        this.ID = ID;
        this.FirstName = FirstName;
        this.MiddleName = MiddleName;
        this.LastName = LastName;
        this.BirthDate = BirthDate;
    }

    public int getID() {
        return ID;
    }
    public void setID(int ID) {
        this.ID = ID;
    }

    public String getFirstName() {
        return FirstName;
    }
    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getMiddleName() {
        return MiddleName;
    }
    public void setMiddleName(String middleName) {
        MiddleName = middleName;
    }

    public String getLastName() {
        return LastName;
    }
    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getBirthDate() {
        return BirthDate;
    }
    public void setBirthDate(String birthDate) {
        BirthDate = birthDate;
    }

    public String getFullName(){
        String FullName = FirstName + " " + MiddleName + " "  + LastName;
        return FullName;
    }
    public String changeDOB(){
        String[] DOB = BirthDate.split("-");
        String DOBchange = DOB[2] + "-" + DOB[0] + "-" + DOB[1];
        return DOBchange;
    }
}
