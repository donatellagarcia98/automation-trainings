package test.java.Academy.Automation;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Test1 {
	
	@Test
	public void Exercise2(){
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\A\\Desktop\\SeleniumCourse\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://formy-project.herokuapp.com/form");
		driver.findElement(By.id("first-name")).sendKeys("Donatella");
		driver.findElement(By.id("last-name")).sendKeys("García");
		driver.findElement(By.id("job-title")).sendKeys("IT Trainee");
		
		driver.findElement(By.cssSelector("div[class=\"input-group\"] div input[id=\"radio-button-2\"]")).click();
		driver.findElement(By.id("checkbox-2")).click();
		
		driver.findElement(By.cssSelector("select[id=\"select-menu\"] option[value=\"1\"]")).click();
		driver.findElement(By.id("datepicker")).click();
		driver.findElement(By.cssSelector("td[class=\"today day\"]")).click();
		
		driver.findElement(By.cssSelector("a[class=\"btn btn-lg btn-primary\"]")).click();		
	}

}
